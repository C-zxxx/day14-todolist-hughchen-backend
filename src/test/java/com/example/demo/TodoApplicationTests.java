package com.example.demo;

import com.example.demo.Entity.Todo;
import com.example.demo.repository.JPATodoListRepository;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import java.util.List;
import java.util.Optional;

import static org.hamcrest.Matchers.containsInAnyOrder;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;

@SpringBootTest
@AutoConfigureMockMvc
class TodoApplicationTests {

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private JPATodoListRepository jpaTodoListRepository;

    @BeforeEach
    void setUp() {
        jpaTodoListRepository.deleteAll();
    }

    @Test
    public void should_return_todo_when_addTodo_given_todo() throws Exception {
        // given
        Todo todo = new Todo("text1");
        ObjectMapper objectMapper = new ObjectMapper();
        String todoJson = objectMapper.writeValueAsString(todo);

        //when
        mockMvc.perform(post("/todos")
                .contentType(MediaType.APPLICATION_JSON)
                .content(todoJson))
                .andExpect(MockMvcResultMatchers.status().is(201))
                .andExpect(MockMvcResultMatchers.jsonPath("$.id").isNumber())
                .andExpect(MockMvcResultMatchers.jsonPath("$.text").value(todo.getText()))
                .andExpect(MockMvcResultMatchers.jsonPath("$.done").value(todo.isDone()));
    }

    @Test
    public void should_return_todoList_when_getAllTodos_given_null() throws Exception {
        // given
        Todo todo1 = new Todo("todo1");
        Todo todo2 = new Todo("todo2");
        jpaTodoListRepository.save(todo1);
        jpaTodoListRepository.save(todo2);

        //when
        mockMvc.perform(get("/todos"))
                .andExpect(MockMvcResultMatchers.status().is(200))
                .andExpect(MockMvcResultMatchers.jsonPath("$.length()").value(2))
                .andExpect(MockMvcResultMatchers.jsonPath("$[0].id").value(todo1.getId()))
                .andExpect(MockMvcResultMatchers.jsonPath("$[0].text").value(todo1.getText()))
                .andExpect(MockMvcResultMatchers.jsonPath("$[0].done").value(todo1.isDone()))
                .andExpect(MockMvcResultMatchers.jsonPath("$[1].id").value(todo2.getId()))
                .andExpect(MockMvcResultMatchers.jsonPath("$[1].text").value(todo2.getText()))
                .andExpect(MockMvcResultMatchers.jsonPath("$[1].done").value(todo2.isDone()))
                .andExpect(MockMvcResultMatchers.jsonPath("$[*].id").value(containsInAnyOrder(todo1.getId().intValue(), todo2.getId().intValue())))
                .andExpect(MockMvcResultMatchers.jsonPath("$[*].text").value(containsInAnyOrder(todo1.getText(), todo2.getText())))
                .andExpect(MockMvcResultMatchers.jsonPath("$[*].done").value(containsInAnyOrder(todo1.isDone(), todo2.isDone())));
    }

    @Test
    void should_delete_todo_by_id() throws Exception {
        Todo todo = new Todo("todo1");
        jpaTodoListRepository.save(todo);

        mockMvc.perform(delete("/todos/{id}", todo.getId()))
                .andExpect(MockMvcResultMatchers.status().is(204));

        assertTrue(jpaTodoListRepository.findById(todo.getId()).isEmpty());
    }

    @Test
    void should_update_todo_when_updateTodo_given_todo() throws Exception {
        Todo todoText = new Todo("todo1");
        Todo todoDone = new Todo("todo2");
        jpaTodoListRepository.save(todoText);
        jpaTodoListRepository.save(todoDone);
        ObjectMapper objectMapper = new ObjectMapper();
        String todoTextJson = objectMapper.writeValueAsString(new Todo("text"));
        String todoDoneJson = objectMapper.writeValueAsString(new Todo(true));

        mockMvc.perform(put("/todos/{id}", todoText.getId())
                .contentType(MediaType.APPLICATION_JSON)
                .content(todoTextJson))
                .andExpect(MockMvcResultMatchers.status().is(200));

        mockMvc.perform(put("/todos/{id}", todoDone.getId())
                .contentType(MediaType.APPLICATION_JSON)
                .content(todoDoneJson))
                .andExpect(MockMvcResultMatchers.status().is(200));


        Optional<Todo> newTodoTextOptional = jpaTodoListRepository.findById(todoText.getId());
        Optional<Todo> newTodoDoneOptional = jpaTodoListRepository.findById(todoDone.getId());

        assertTrue(newTodoTextOptional.isPresent());
        assertTrue(newTodoDoneOptional.isPresent());

        Todo newTodoText = newTodoTextOptional.get();
        Todo newTodoDone = newTodoDoneOptional.get();

        assertEquals("text", newTodoText.getText());
        assertTrue(newTodoDone.isDone());

    }

    @Test
    void should_find_todo_by_id() throws Exception {
        Todo todo = new Todo("todo");
        jpaTodoListRepository.save(todo);

        mockMvc.perform(get("/todos/{id}", todo.getId()))
                .andExpect(MockMvcResultMatchers.status().is(200))
                .andExpect(MockMvcResultMatchers.jsonPath("$.id").value(todo.getId()))
                .andExpect(MockMvcResultMatchers.jsonPath("$.text").value(todo.getText()))
                .andExpect(MockMvcResultMatchers.jsonPath("$.done").value(todo.isDone()));
    }


}
