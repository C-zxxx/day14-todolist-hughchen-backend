package com.example.demo.repository;

import com.example.demo.Entity.Todo;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface JPATodoListRepository extends JpaRepository<Todo, Long> {
}
