package com.example.demo.service;

import com.example.demo.Entity.Todo;
import com.example.demo.exception.TodoNotFoundException;
import com.example.demo.repository.JPATodoListRepository;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class TodoService {
    private final JPATodoListRepository jpaTodoListRepository;

    public TodoService(JPATodoListRepository jpaTodoListRepository) {
        this.jpaTodoListRepository = jpaTodoListRepository;
    }

    public List<Todo> findAll() {
        return jpaTodoListRepository.findAll();
    }

    public Todo save(Todo todo) {
        return jpaTodoListRepository.save(todo);
    }

    public void delete(Long id) {
        jpaTodoListRepository.deleteById(id);
    }

    public void updateTodo(Long id, Todo todo) {
        Todo todoToBeUpdate = jpaTodoListRepository.findById(id).orElseThrow(TodoNotFoundException::new);
        if (todoToBeUpdate.getText() != null) {
            todoToBeUpdate.setText(todo.getText());
        }
        if (todoToBeUpdate.isDone()) {
            todoToBeUpdate.setDone(todo.isDone());
        }
        jpaTodoListRepository.save(todoToBeUpdate);
    }

    public Todo findById(Long id) {
        return jpaTodoListRepository.findById(id).orElseThrow(TodoNotFoundException::new);
    }
}
